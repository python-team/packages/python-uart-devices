Source: python-uart-devices
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Edward Betts <edward@4angle.com>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-poetry-core,
Build-Depends-Indep:
 python3-pytest <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-pytest-cov <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.7.1
Homepage: https://github.com/Bluetooth-Devices/uart-devices
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-uart-devices
Vcs-Git: https://salsa.debian.org/python-team/packages/python-uart-devices.git

Package: python3-uart-devices
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Python library for managing UART devices on Linux
 uart-devices is a comprehensive Python library designed to interact with
 UART (Universal Asynchronous Receiver/Transmitter) devices on Linux platforms.
 This library simplifies the process of detecting, configuring, and managing
 UART devices directly from Python scripts, providing a robust toolkit for
 developers working with serial communication hardware.
 .
 The library enables the enumeration of UART devices, reading device-specific
 attributes such as manufacturer and product details, and setting up devices
 for communication. It includes support for Bluetooth devices that use UART for
 communication, allowing for seamless integration and management of such
 devices.
 .
 Key features include:
 .
  - Automatic detection and initialization of UART and Bluetooth UART devices.
  - Easy retrieval of device information, including manufacturer and product
    names.
  - Asynchronous API support for efficient device setup and management.
  - Custom exception handling for better error management and debugging.
